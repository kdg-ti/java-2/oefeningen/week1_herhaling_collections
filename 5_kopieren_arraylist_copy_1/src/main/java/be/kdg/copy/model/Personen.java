package be.kdg.copy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Werk de nodige methoden verder uit.
 */
public class Personen {
    private List<Persoon> personen;

    public Personen() {
        personen = new ArrayList<>();
    }

    public Personen(List<Persoon> anderePersonen) {

    }

    public void voegPersoonToe(Persoon persoon) {

    }

    public String maakPersonenString() {
        return null;
    }

    /*
     * De kopie wordt gemaakt in de tweede constructor!
     */
    public Personen maakKopie() {
       return new Personen(personen);
    }
}
