package kdg.copy.model;

/**
 * Werk de nodige methoden verder uit.
 */
public class Personen {
    private static final int MAX_PERSONEN = 10;

    private final Persoon[] personen;
    private int aantal;

    private Personen(Persoon[] personen, int aantal) {
        this.personen = personen;
        this.aantal = aantal;
    }

    public Personen() {
        personen = new Persoon[MAX_PERSONEN];
    }

    public void voegPersoonToe(Persoon persoon) {

    }

    public String maakPersonenString() {
       return null;
    }

    /*
     * TODO 1. Maak hierin gebruik van de methode System.arraycopy
     * om een kopie van de tabel te maken
     * en test.
     */
    public Personen maakKopie() {
        return null;
    }

	/*
* TODO 2 Als de test met TODO 1 geslaagd is, implementeer dan maakKopie2
* Maak hier gebruik van de methode  Arrays.copyOf  (sinds Java 6)
* Zie Javadoc
*/
	public Personen maakKopie2() {
		return  null;
	}
}
