package be.kdg.interfacedemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Vul aan waar nodig. Maak gebruik van de Sorteerder interface.
 */
public class Getallen {
    private List getallen;

    /* Maak hier de ArrayList */
    public Getallen(List getallen) {

    }

    /* Kopieer hier de tabel van int in de ArrayList
     * Maak gebruik van new Getal(...) */
    public Getallen(int[] getallen) {

    }

    /* Sorteer met de sort methode uit de Collections klasse */
    public void sort() {

    }

    /* Sorteer met de sort methode uit de Collections klasse
     * Gebruik als tweede parameter een specifieke methode,
     * Zie hiervoor de klasse Collections
     */
    public void sortReversed() {

    }

    /* Zorg ervoor dat de gewenste afdruk op het scherm komt */
    public String toString() {
        return "nonsens";
    }
}

