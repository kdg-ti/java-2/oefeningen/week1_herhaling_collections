package be.kdg.interfacedemo;

/**
 * Vul aan waar nodig.
 *
 * Maak gebruik van zowel de Sorteerbaar als de Comparable interface.
 */
public final class Getal {
    private final int getal;

    public Getal(int getal) {
        this.getal = getal;
    }

    /* Deze methode geeft true terug als het eerste getal
     * kleiner is dan het tweede.
     */
    public boolean kleinerDan(Sorteerbaar sorteerbaar) {
        return false;
    }

    /* Deze methode doet net het omgekeerde */
    public boolean groterDan(Sorteerbaar sorteerbaar) {
        return false;
    }

    /* Hier zet je een getalwaarde om naar een String, reeds uigewerkt. */
    public String toString() {
        return Integer.toString(getal);
    }


}
