package be.kdg.interfacedemo;

/**
 * Vul aan waar nodig.
 *
 * Maak gebruik van zowel de Sorteerbaar als de Comparable interface.
 */
public final class Woord  {
    private final String woord;

    public Woord(String woord) {
        this.woord = woord;
    }

    /* Deze methode geeft true terug als het eerste woord alfabetisch
     * voor het tweede komt.
     */
    public boolean kleinerDan(Sorteerbaar sorteerbaar) {
        return false;
    }

    /* Deze methode doet het omgekeerde van de voorgaande */
    public boolean groterDan(Sorteerbaar sorteerbaar) {
        return false;
    }

    public String toString() {
        return woord;
    }
}
